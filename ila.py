import os
import json
import argparse

ilatemplate = """create_ip -name ila -vendor xilinx.com -library ip -version 6.2 -module_name %(modulename)s
set_property -dict {
%(cfgstr)s
} [get_ips %(modulename)s]
#generate_target {instantiation_template} [get_files %(modulename)s.xci]
#generate_target all [get_files  %(modulename)s.xci]
add_files [get_files [list %(modulename)s.xci]]
#export_ip_user_files -of_objects [get_files %(modulename)s.xci] -no_script -sync -force -quiet
#create_ip_run [get_files -of_objects [get_fileset sources_1] %(modulename)s.xci]
#CONFIG.C_EN_STRG_QUAL {1}
#CONFIG.C_TRIGIN_EN {true}
"""
ilainsttemplate = """generate
if (DEBUG=="true") begin
    %(modulename)s %(modulename)s(%(probes)s
    );
end
endgenerate
"""
ilasimtemplate = """module %(modulename)s (input clk
,%(simstr)s
);
endmodule
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(help="ila json filename", dest="jsonfilename", type=str)
    parser.add_argument("-t", help="ila tcl filename", dest="tclfilename", type=str)
    parser.add_argument("-v", help="ila vh filename", dest="vhfilename", type=str)
    parser.add_argument("-s", help="ila sim filename", dest="vsimfilename", type=str)
    parser.add_argument("-d", help="ila data depth default 1024", dest="datadepth", type=int, default=1024)
    parser.add_argument("-o", help="output path", dest="outputpath", type=str, default='.')
    clargs = parser.parse_args()
    with open(clargs.jsonfilename) as ilajsonfile:
        ilajson = json.load(ilajsonfile)
    modulename = os.path.splitext(os.path.basename(clargs.jsonfilename))[0]
    configstr = ["CONFIG.C_DATA_DEPTH {%d}" % clargs.datadepth]
    typewidth = []
    probestr = [".clk(%(clk)s)" % (dict(clk=ilajson["clk"]))]
    simstr = []
    index = 0
    for probename, width in ilajson["probes"].items():
        pdict = dict(probename=probename, width=int(width), index=index)
        configstr.append("CONFIG.C_PROBE%(index)d_TYPE {0}" % pdict)
        configstr.append("CONFIG.C_PROBE%(index)d_WIDTH {%(width)d}" % pdict)
        probestr.append(".probe%(index)d(%(probename)s)  // WIDTH %(width)d" % pdict)
        simstr.append("input [%(width)d-1:0] probe%(index)d" % pdict)
        index = index + 1
    configstr.append("CONFIG.C_NUM_OF_PROBES {%d}" % (index))
    with open(os.path.join(clargs.outputpath,modulename + ".tcl"), "w+") as tclfile:
        tclfile.write(ilatemplate % (dict(cfgstr="\n".join(configstr), modulename=modulename)))
    with open(os.path.join(clargs.outputpath,modulename + ".vh"), "w+") as vhfile:
        vhfile.write(ilainsttemplate % (dict(probes="\n,".join(probestr), modulename=modulename)))
    with open(os.path.join(clargs.outputpath,modulename + ".v"), "w+") as vhfile:
        vhfile.write(ilasimtemplate % (dict(simstr="\n,".join(simstr), modulename=modulename)))

#    f=open('lbread.vh','w+')
#    f.write('\n'.join(readstr))
#    f.close()
# print(probestr)
