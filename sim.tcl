proc sim {target tend} {
#set_property top_lib xil_defaultlib [get_filesets sim_1]
#set_property top ${target}_tb [current_fileset]
set_property top ${target}_tb [get_filesets sim_1]
set_property -name {xsim.simulate.runtime} -value {000ns} -objects [current_fileset -simset]
launch_simulation
open_vcd
log_vcd
puts ${tend}
run ${tend}
close_vcd
close_sim
}
