import os
import argparse
import git


def gititer(path, action="autocommit", AUTOCOMMITMSG="Makefileautomatedcommit", patchdict={},outputdir="."):
    repo = git.Repo(path, search_parent_directories=True)
    rootpath = repo.git.rev_parse("--show-toplevel")
    for m in repo.submodules:
        subpath = rootpath + "/" + m.path
        print(subpath)
        try:
            _ = git.Repo(subpath)
            print("submodule:", m.name)  # ,s.head.commit.message)
            gititer(subpath, action=action, patchdict=patchdict,outputdir=outputdir)
        except Exception as e:
            print("Warning: failed processing %s" % subpath)
            print(f"Error in gititer: {e}")
    if action == "autocommit":
        gitautocommit(repo, AUTOCOMMITMSG=AUTOCOMMITMSG)
    elif action == "list":
        gitlist(repo)
        pass
    elif action == "preppush":
        gitpreppush(repo, AUTOCOMMITMSG=AUTOCOMMITMSG, patchdict=patchdict)
    elif action == "push":
        gitpush(repo, AUTOCOMMITMSG=AUTOCOMMITMSG)
        pass
    elif action == "revisionverilog":
        gitrevisionverilog(repo,outputdir=outputdir)
    elif action == "diff":
        print(gitdiff(repo))
    else:
        pass


def gitdiff(repo):
    t = repo.head.commit.tree
    return repo.git.diff(t)  # 'HEAD~1')


def gitlist(repo):
    print(repo.head.commit.message)


def gitpush(repo, AUTOCOMMITMSG):
    if AUTOCOMMITMSG in repo.head.commit.message:
        print(repo.working_dir)
        print(
            """NOT PUSHED: Do not push the automated commit to the repo, update the git message by "git commit -a --amend" and edit the message"""
        )
    else:
        print("\t", repo.working_dir)
        origin = repo.remote(name="origin")
        origin.push()


def gitautocommit(repo, AUTOCOMMITMSG):
    options = ["-a"]
    print("processing repo", repo, repo.working_tree_dir)
    if repo.is_dirty():
        diffstr = gitdiff(repo)
        print(diffstr)
        if AUTOCOMMITMSG in repo.head.commit.message:
            options.append("--amend")
        usermsg = input("%s commit message:" % (repo.working_dir))
        options.append("-m %s: %s" % (AUTOCOMMITMSG, usermsg))
        try:
            repo.git.commit(*options)
        except Exception:
            pass
        print(repo.head.commit.message)


def gitpreppush(repo, AUTOCOMMITMSG, patchdict={}):
    print(repo.working_tree_dir)
    if repo.working_tree_dir in patchdict:
        if AUTOCOMMITMSG in repo.head.commit.message:
            for c in repo.iter_commits():
                if AUTOCOMMITMSG not in c.message:
                    diffstr = repo.git.diff(c.tree)
                    try:
                        if any(branch.name == AUTOCOMMITMSG for branch in repo.heads):
                            #                            repo.git.merge("-m",AUTOCOMMITMSG)
                            repo.git.checkout(AUTOCOMMITMSG)
                            repo.git.merge("HEAD", "-m", AUTOCOMMITMSG)
                        else:
                            repo.git.branch(AUTOCOMMITMSG)
                        repo.git.checkout(c)
                    except Exception as e:
                        print(f"Error while checking out commit: {e}")
                    break
                # test4
            patchfile = patchdict[repo.working_tree_dir]
            writediff = True
            if os.path.exists(patchfile):
                with open(patchfile) as rfile:
                    sfile = rfile.read()
                if diffstr == sfile:
                    writediff = False
            if writediff:
                with open(patchfile, "w") as fwrite:
                    fwrite.write(diffstr)


def gitrevisionverilog(repo,outputdir='.'):
    template = """module gitrevision(output [31:0] git);
assign git=32'h%s;
endmodule
""" % (
        repo.commit().hexsha[0:8]
    )
    f = open(os.path.join(outputdir,"gitrevision.v"), "w")
    f.write(template)
    f.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-action", "--action", help="action: autocommit, list", dest="action", type=str, default="autocommit"
    )
    parser.add_argument("-patchinsteadofcommit", help="patch instead of commit", dest="patchstr", type=str, nargs="*")
    parser.add_argument("-dir", "--dir", help="git location", dest="dir", type=str, default=".")
    parser.add_argument("-outputdir", "--outputdir", help="output directory", dest="outputdir", type=str, default=".")
    clargs = parser.parse_args()
    patchdict = {}
    if clargs.patchstr:
        for kv in clargs.patchstr:
            k, v = kv.split("=")
            if os.path.exists(k):
                absk = os.path.abspath(k)
                absv = os.path.abspath(v)
                patchdict[absk] = absv
    gititer(os.path.abspath(clargs.dir), action=clargs.action, patchdict=patchdict,outputdir=clargs.outputdir)
