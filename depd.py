import os
import re
import argparse

class FileProcessor:
    def __init__(self, filenames, exclude=[]):
        self.files=[]
        for filename in filenames:
            self.filename = os.path.basename(filename)
            self.base_dir = os.path.dirname(filename)
            self.exclude = exclude
            self.files.extend(self.depd(self.filename, self.base_dir))

    def depd(self, filename_d, dirin="./"):
        with open(os.path.join(dirin, filename_d)) as topd:
            files = []
            for filepath in topd.read().split("\n"):
                if filepath:
                    abspath = filepath if os.path.isabs(filepath) else os.path.abspath(os.path.join(dirin, filepath))
                    directory = os.path.dirname(os.path.join(dirin, filepath)) + "/"
                    filename = os.path.basename(os.path.join(dirin, filepath))
                    if filename not in self.exclude:
                        filename, ext = os.path.splitext(filename)
                        if ext in [".d", ".lib"]:
                            files.extend(self.depd(abspath, dirin=directory))
                        else:
                            files.append(abspath)
        return list(set(files))

    def extdict(self, filelist):
        edict = {}
        for f in filelist:
            ext = os.path.splitext(f)[-1]
            if ext not in edict:
                edict[ext] = []
            edict[ext].append(f)
        return edict

    def typefile(self, extdict, typelist):
        olist = []
        for t in typelist:
            if t in extdict:
                olist.extend(extdict[t])
        return olist

    def vhpaths(self, extdict):
        vhpathlist = []
        for f in self.typefile(extdict, [".vh"]):
            path, base = os.path.split(f)
            if path not in vhpathlist:
                vhpathlist.append(path)
        return vhpathlist

    def yvhpaths(self):
        return " ".join(["-y %s" % i for i in self.vhpaths(self.extdict(self.files))])

    def vsv(self):
        return self.typefile(self.extdict(self.files), [".v", ".sv"])

    def tcldict(self):
        edict = self.extdict(self.files)
        src = self.typefile(edict, [".v", ".sv", ".vhd", ".hex"])
        constrain = self.typefile(edict, [".xdc"])
        tcl = self.typefile(edict, [".tcl"])
        vh = self.vhpaths(edict)
        unk = [f for f in self.files if f not in src + constrain + tcl + vh]

        data = [src, constrain, tcl, vh, unk]
        tcl_dict = self._to_tcl_dict(data)
        tcl_dict_str = 'dict create '
        for key, value in tcl_dict.items():
            tcl_dict_str += '{} {{{}}} '.format(key, ' '.join(value))
        return tcl_dict_str

    def _to_tcl_dict(self, data):
        tcl_dict = {}
        keys = ['src', 'constrain', 'tcl', 'vh', 'unk']
        for key, value in zip(keys, data):
            tcl_dict[key] = value
        return tcl_dict

    def process(self, action,outputfile=None):
        returnstr=''
        if action is None:
            returnstr="\n".join(self.files)
        elif action == "vsv":
            returnstr="\n".join(self.vsv())
        elif action == "yvh":
            returnstr=self.yvhpaths()
        elif action == "tcldict":
            returnstr=self.tcldict()
        elif action == "relative":
            returnstr='\n'.join(sorted([os.path.relpath(f,os.getcwd()) for f in self.files]))
        else:
            pass
        if outputfile is not None:
            with open(outputfile,'w') as ofile:
                ofile.write(returnstr)
        else:
            print(returnstr)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dfile", help="dependent .d file",nargs="+")
    parser.add_argument("-o","-outputfile",dest="outputfile", help="output file")
    parser.add_argument(
        "-a",
        "--action",
        help="Action to perform: vsv for .v and .sv, yvh for -y vhpath, tcldict for Tcl dictionary, relative for relative to current directory",
        dest="action",
        type=str,
        choices=["vsv", "yvh", "tcldict", "relative"],
        default=None,
    )
    parser.add_argument(
        "-e", "--exclude", help="File list to be excluded", dest="exclude", type=str, nargs="+", default=[]
    )
    clargs = parser.parse_args()
    processor = FileProcessor(filenames=clargs.dfile, exclude=clargs.exclude)
    processor.process(clargs.action,outputfile=clargs.outputfile)
