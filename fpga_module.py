import re
import sys
from hw_component import c_pin


class fpga_pin(c_pin):
    def __init__(self, pin, pinname, memory_byte_group, bank, vccaux_group, super_logic_region, iotype, no_connect):
        c_pin.__init__(self, pin, pinname)
        # self.pin=pin
        # self.pinname=pinname
        self.memory_byte_group = memory_byte_group
        self.bank = bank
        self.vccaux_group = vccaux_group
        self.super_logic_region = super_logic_region
        self.iotype = iotype
        self.no_connect = no_connect
        self.alias = []

    def rename(self, newname):
        self.alias.append(self.pinname)
        self.pinname = newname


class xilinx_pinmap:
    def __init__(self, filename):
        self.pins = self.parse_file(filename)

    def parse_file(self, filename):
        f = open(filename)
        s = f.read()
        f.close()
        pins = []
        pinscount = 0
        s1 = re.sub(r"\/\*([\s\S]*)?\*\/|//.*?(\r\n?|\n)", "", s)
        for l in s1.split("\n"):
            mtitle = re.match(r"Device/Package\s*(\S*)\s*(\S*)\s*(\S*)\s*", l)
            mhead = re.match(
                r"Pin\s*Pin Name\s*Memory Byte Group\s*Bank\s*VCCAUX Group\s*Super Logic Region\s*I/O Type\s*No-Connect\s*",
                l,
            )
            mpin = re.match(r"(\S+\d+)\s+(\S*)\s+(\S+)\s+(\S+)\s+(\S*)\s+(\S+)\s+(\S+)\s+(\S+)\s*", l)
            mtail = re.match(r"Total Number of Pins Generated,\s*(\d+)", l)
            if mtitle:
                self.package = mtitle.group(1)
                self.date = mtitle.group(2)
                self.time = mtitle.group(3)
            elif mhead:
                pass
            elif mpin:
                pins.append(
                    fpga_pin(
                        pin=mpin.group(1),
                        pinname=mpin.group(2),
                        memory_byte_group=mpin.group(3),
                        bank=mpin.group(4),
                        vccaux_group=mpin.group(5),
                        super_logic_region=mpin.group(6),
                        iotype=mpin.group(7),
                        no_connect=mpin.group(8),
                    )
                )
                pinscount = pinscount + 1
            elif mtail:
                if pinscount != eval(mtail.group(1)):
                    print(
                        "warning: pin number %d doesn't match specified in file %d"
                        % (pinscount, eval(mtail.group(1)))
                    )
            else:
                pass
        #                print(l)
        return pins

    def property_match(self, obj, criteria):
        result = False
        for prop, value in criteria.items():
            result = result or getattr(obj, prop) in value
        return result

    def verilog_io_pin(self, notinverilog={"iotype": ["CONFIG", "NA"]}):
        # io_pin=[pin for pin in self.pins if not (pin.iotype in notinverilog)]
        io_pin = [pin for pin in self.pins if not (self.property_match(pin, notinverilog))]
        return io_pin

    # def verilog_top(self,pins):
    #     ucfs=[]
    #     for pin in pins:
    #         #            print pin.pin
    #         ucfs.append(pin.ucf())
    #     return ['\n'.join(xdcs),'\n'.join(ucfs),',\n'.join(verilogs)]
    def xdcs_gen(self, pins, filename):
        xdcs = []
        for pin in pins:
            xdcs.append(pin.xdc())

        f = open(filename, "w")
        f.write(",\n".join(xdcs))
        f.close()

    def verilog_gen(self, pins, filename, name, body):
        verilog_ports = []
        for pin in pins:
            verilog_ports.append(pin.verilog())
        port = ",\n\t".join(verilog_ports)
        f = open(filename, "w")
        f.write(
            """module %s (
    %s
);
%s
endmodule"""
            % (name, port, body)
        )
        f.close()

    def svxdc(self, yespins, nopins=[], defaultiostandard="LVDS"):
        yestemp = """set_property PACKAGE_PIN %(package_pin)s [get_ports {fpga\\\\.%(package_pin)s}];# %(comment)s"""
        notemp = """#set_property PACKAGE_PIN %(package_pin)s [get_ports {fpga\\\\.%(package_pin)s}];# %(comment)s"""
        xdclist = [yestemp % (dict(package_pin=p.package_pin, comment=p.pinname)) for p in yespins]
        xdclist.extend([notemp % (dict(package_pin=p.package_pin, comment=p.pinname)) for p in nopins])
        xdclist.append("set_property BITSTREAM.GENERAL.UNCONSTRAINEDPINS Allow [current_design]")
        # xdclist.append('''set_property IOSTANDARD %s [get_ports -of_objects [get_iobanks -filter { BANK_TYPE !~  "BT_MGT" }]]'''%defaultiostandard)
        return "\n".join(xdclist)

    # gtxiopins=[pin for pin in pins if pin.iotype=='GTX' and ('TX' in pin.pinname or 'RX' in pin.pinname)]
    #    iopins=[pin for pin in pins if not (pin.iotype=='GTX' and ('TX' in pin.pinname or 'RX' in pin.pinname))]
    #
    #        print('\n'.join([('#' if p.package_pin in exceptlist else '' ) + xdctemp%(dict(package_pin=p.package_pin,comment=p.pinname)) for p in iopins if p]))
    #        print('\n'.join([('#' if p.package_pin in exceptlist else '' ) + xdctemp%(dict(package_pin=p.package_pin,comment=p.pinname)) for p in gtxiopins]))
    #        print('set_property BITSTREAM.GENERAL.UNCONSTRAINEDPINS Allow [current_design]')
    #        print(dir(pins[0]))
    def pininterface(self, yespins, nopins, yesport, noport):
        piniftemp = r"""interface xc7vx485tffg1761pkg #(parameter USEREFPIN="false")();
%s
%s
/*modport top(
%s
%s
);
*/
endinterface"""
        yesstr = "wire %s;" % (",".join([p.package_pin for p in yespins]))
        nostr = "\n".join([r"//wire %s; // %s" % (p.package_pin, p.pinname) for p in nopins])
        yesportstr = "inout %s" % (",".join([p.package_pin for p in yesport]))
        noportstr = "\n".join([r"//wire %s; // %s" % (p.package_pin, p.pinname) for p in noport])
        return piniftemp % (yesstr, nostr, yesportstr, noportstr)


if __name__ == "__main__":
    fpga = xilinx_pinmap(sys.argv[1])
    fpga.svxdc_gen(
        fpga.verilog_io_pin(),
        "t2.xdc",
        exceptlist=[
            "A12",
            "A5",
            "A6",
            "AA41",
            "AL40",
            "AP37",
            "B11",
            "B18",
            "B24",
            "C41",
            "D12",
            "D31",
            "H35",
            "H7",
            "H8",
            "L11",
            "P17",
        ],
    )
#    fpga.verilog_gen(fpga.verilog_io_pin(),'t2.v','t2','')
#    fpga.xdcs_gen(fpga.verilog_io_pin(),'t2.xdc')
#    [xdc,ucf,verilog]=fpga.verilog_top(fpga.verilog_io_pin())
#    print xdc
#    print verilog
