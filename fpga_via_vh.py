template = {
    "all": """{inperbus}\n{outperbus}\n{viaperbustemp}""",
    "viaperbustemp": """viapairs #(.WIDTH({pincount}))
viapairs ({{
{viaperbus}
}}
);
""",
    "viaperbus": "fpga_{pin},fpga.{Pin}",
    "inperbus": "assign fpga.{Pin}=fpga_{pin};",
    "outperbus": "assign fpga_{pin}=fpga.{Pin};",
}


def inperbus(pindict):
    return "\n".join(sorted([template["inperbus"].format(**p) for k, p in pindict.items()]))


def outperbus(pindict):
    return "\n".join(sorted([template["outperbus"].format(**p) for k, p in pindict.items()]))


def viaperbus(pindict):
    return "\n,".join(sorted([template["viaperbus"].format(**p) for k, p in pindict.items()]))


def viaperbustemp(pindict):
    if len(pindict) > 0:
        retstr = template["viaperbustemp"].format(**(dict(viaperbus=viaperbus(pindict), pincount=len(pindict))))
    else:
        retstr = ""
    return retstr


def all(pindict):
    vialist = {k: v for k, v in pindict.items() if v["direction"] == "inout"}
    inlist = {k: v for k, v in pindict.items() if v["direction"] == "in"}
    outlist = {k: v for k, v in pindict.items() if v["direction"] == "out"}
    return template["all"].format(
        **(
            dict(
                viaperbustemp=viaperbustemp(vialist),
                pincount=len(vialist),
                inperbus=inperbus(inlist),
                outperbus=outperbus(outlist),
            )
        )
    )


def allsim(pindict):
    vialist = {k: v for k, v in pindict.items() if v["direction"] == "inout"}
    outlist = {k: v for k, v in pindict.items() if v["direction"] == "in"}
    inlist = {k: v for k, v in pindict.items() if v["direction"] == "out"}
    return template["all"].format(
        **(
            dict(
                viaperbustemp=viaperbustemp(vialist),
                pincount=len(vialist),
                inperbus=inperbus(inlist),
                outperbus=outperbus(outlist),
            )
        )
    )


def writefile(pindict, filename="fpga_via.vh", filename_sim="fpga_via_sim.vh"):
    with open(filename, "w") as f:
        f.write(all(pindict))
    with open(filename_sim, "w") as f:
        f.write(allsim(pindict))
