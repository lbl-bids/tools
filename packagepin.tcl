proc packagepins { packagepindict gtpinlist } {
	set result_lines {}
	foreach pin [get_ports] {
		if { [dict exist $packagepindict $pin] } {
			set packagepin [dict get $packagepindict $pin]
			#puts $pin
			#puts $packagepin
			puts "clear packagepin $pin $packagepin"
			set_property PACKAGE_PIN "" [get_ports $pin]
		} else {
			puts $pin
			puts "not in dict"
		}

	}
	foreach pin [get_ports] {
		if { [dict exist $packagepindict $pin] } {
			set packagepin [dict get $packagepindict $pin]
			if { [lsearch -exact $gtpinlist $packagepin] >= 0 } {
				puts IS_GT_TERM:[get_property IS_GT_TERM [get_ports $pin]]
				puts LOGIC_VALUE:[get_property LOGIC_VALUE [get_ports $pin]]
				puts $pin
				if { [get_property LOGIC_VALUE [get_ports $pin]] == "unknown" } {
					set_property PACKAGE_PIN $packagepin [get_ports $pin]
					puts "set packagepin $pin $packagepin"
					set property_line "set_property PACKAGE_PIN $packagepin \[get_ports $pin\]"
					lappend result_lines $property_line
				} else {
					disconnect_net -objects [get_ports $pin]
					remove_port $pin
					puts "remove unused gt port $pin"
				}

			} else {
				puts "set packagepin $pin $packagepin"
				set_property PACKAGE_PIN $packagepin [get_ports $pin]
				set property_line "set_property PACKAGE_PIN $packagepin \[get_ports $pin\]"
				lappend result_lines $property_line
			}
		}
	}
	return $result_lines
}
#		set packagepin [dict get $packagepindict $pin]
#		puts "set packagepin $pin $packagepin"
#		set_property PACKAGE_PIN $packagepin [get_ports $pin]
#				if { [get_property IS_GT_TERM [get_ports $pin]] == 1 } {
#					set_property PACKAGE_PIN $packagepin [get_ports $pin]
#					puts $packagepin
#				} else {
#					disconnect_net -objects [get_ports $pin]
#					remove_port $pin
#					puts "remove unused gt port"
#					puts $pin
#					puts "removed"
##					set_property IO_BUFFER_TYPE none [get_ports $pin]
#				}
