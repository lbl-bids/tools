template = {
    "all": "{perbus}",
    "perbus": "{dirput} fpga_{pin}",
}


def perbus(pindict):
    return "\n,".join(sorted([template["perbus"].format(**p) for k, p in pindict.items()]))


def all(pindict):
    return template["all"].format(**dict(perbus=perbus(pindict)))


def writefile(pindict, filename="fpga_port.vh"):
    with open(filename, "w") as f:
        f.write(all(pindict))
